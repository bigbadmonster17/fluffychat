import 'package:flutter/material.dart';

import 'package:matrix/matrix.dart';

import 'package:fluffychat/utils/matrix_sdk_extensions.dart/event_extension.dart';
import 'package:fluffychat/widgets/matrix.dart';

class MessageDownloadContent extends StatelessWidget {
  final Event event;
  final Color textColor;

  const MessageDownloadContent(this.event, this.textColor, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String filename = event.content.containsKey('filename')
        ? event.content['filename']
        : event.body;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        TextButton.icon(
          onPressed: () => event.saveFile(context),
          icon: const Icon(Icons.download_outlined),
          label: Text(filename),
          style: event.senderId == Matrix.of(context).client.userID
              ? TextButton.styleFrom(primary: textColor)
              : null,
        ),
        if (event.sizeString != null)
          Text(
            event.sizeString,
            style: TextStyle(
              color: textColor,
            ),
          ),
      ],
    );
  }
}
